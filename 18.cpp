#include <iostream>
#include <string>

class stack
{
private:
	int last_index = 0;
	int capacity = 0;
	int* mas = new int[capacity];
public:
	stack() : capacity(10)
	{}
	stack(int _capacity) : capacity(_capacity)
	{}
	void push(int item)
	{
		if (last_index < capacity)
		{
			mas[last_index] = item;
			last_index++;
		}
	}
	int pop()
	{
		if (last_index > 0)
		{
			last_index--;
			return mas[last_index];
		}
	}
	~stack()
	{
		delete[] mas;
	}
};


int main()
{
	stack array = stack(3);
	array.push(7);
	std::cout << array.pop() << '\n';
}